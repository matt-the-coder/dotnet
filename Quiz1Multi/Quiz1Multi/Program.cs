﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Device.Location;

namespace Quiz1Multi
{
    public class InvalidDataException : Exception
    {
        public InvalidDataException() { }
        public InvalidDataException(string msg) : base(msg) { }
        public InvalidDataException(string msg, Exception orig) : base(msg, orig) { }
    }

    public class Airport
    {
        //constructor
        Airport(string code, string city, double lat, double lng, int elevM) //: base()
        {
            Code = code;
            City = city;
            Latitude = lat;
            Longitude = lng;
            elevationMeters = elevM;

        } 


       public Airport(string line)
        {
            string[] data = line.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidDataException("Line for Airport must have exactly 5 values");
            }
            if (data[0] != "Airport")
            { // this check is redundant and we still want to have it
                throw new InvalidDataException("Line must be for Airport type");
            }

            Code = data[1];
            City = data[2];
            string latStr = data[4];
            string lngStr = data[5];
            string eleStr = data[6];
            try
            {
                Latitude = double.Parse(latStr);
                Longitude = double.Parse(lngStr);
                elevationMeters = int.Parse(eleStr);

            }
            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                { // exception chaining (translate one exception into another)
                    throw new InvalidDataException("Integer value expected", ex);
                }
                else throw ex;
            }
        }

        public Airport()
        {
        }

        //code getter/setter
        private string _code;
        //private char letter = Char.TryParse(Console.ReadLine(), out letter);
        public string Code // exactly 3 uppercase letters
        {
            get
            {
                return _code;
            }
            set
            {
                char letter;
                Char.TryParse(Console.ReadLine(), out letter);
                if (value.Length< 3 || value.Length> 3 || value.Contains(";") || char.IsLower(letter))
                {
                    throw new InvalidDataException("Code must be 3 uppercase letters, no semicolons");
                }
        _code = value;
            }
        }

        //city getter/setter
        private string _city;
        public string City // 1-50 characters, no semicolons
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    throw new InvalidDataException("City must be 1-50 characters long, no semicolons");
                }
                _city = value;
            }
        }

        //Latidude getter/setter
        private double _latidude;
        public double Latitude// -90 to 90
        {
            get
            {
                return _latidude;
            }
            set
            {
                if (value < -90 || value > 90)
                {
                    throw new InvalidDataException("Latidude must be -90 to 90");
                }
                _latidude = value;
            }
        }

        //Longitude getter/setter
        private double _longitude;
        public double Longitude // -180 to 180
        {
            get
            {
                return _longitude;
            }
            set
            {
                if (value< -180 || value> 180)
                {
                    throw new InvalidDataException("Longitude must be -180 to 180");
                }
                _longitude = value;
            }
        }

        //Elevation getter/setter
        private int _elevationMeters;
        public int elevationMeters //-1000 to 10000
        {
            get
            {
                return _elevationMeters;
            }
            set
            {
                if (value < -1000 || value > 10000)
                {
                    throw new ArgumentException("Elevation must be -1000 to 10000");
                }
                _elevationMeters = value;
            }
        }

        public override string ToString()
        {
            return $"{Code} in {City} at {Latitude} lat / {Longitude} lng at {elevationMeters}m elevation";
        }

        public string ToDataString()
        {
            return $"{Code};{City};{Latitude};{Longitude};{elevationMeters}";
        }
    }//end Airport class
    

    public class SuperFib
    {

        public static int GetNthFibonacci(int n)
        {
            int number = n - 1; //Need to decrement by 1 since we are starting from 0  
            int[] Fib = new int[number + 1];
            Fib[0] = 0;
            Fib[1] = 1;
            Fib[2] = 2;
            for (int i = 2; i <= number; i++)
            {
                Fib[i] = Fib[i - 2] + Fib[i - 1] + Fib[i - 0];
            }
            return Fib[number];
        }

    }


    class Program
    {

        const string DataFileName = @"..\..\data.txt";
        static List<Airport> AirportsList = new List<Airport>();

        public delegate void LoggerDelegate(string msg);

        public static void LogToConsole(string m)
        {
            Console.WriteLine("Log: " + m);
        }
        public static void LogToFile(string message)
        {
            string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            File.AppendAllText(@"..\..\log.txt", $"{now}: {message}{Environment.NewLine}");
            DateTime dt = new DateTime();
        }

        static void ReadDataFromFile()
        {
            // open file and parse items, add to list
            string[] linesArray = File.ReadAllLines(@"..\..\data.txt");
            foreach (string line in linesArray)
            {
                try
                {
                    string typeName = line.Split(';')[0];
                    switch (typeName)
                    {
                        case "Airport":
                            Airport airport = new Airport(line);
                            AirportsList.Add(airport);
                            break;
                        default:
                            Console.WriteLine("Error in data line: don't know how to make " + typeName);
                            break;
                    }
                }
                catch (InvalidDataException ex)
                {
                    Console.WriteLine("Error parsing line: " + ex.Message);
                }
            }
        }

        static void WriteDataToFile()
        {
            List<string> linesList = new List<string>();
            foreach (var a in AirportsList)
            {
                linesList.Add(a.ToDataString());
            }
            try
            {
                File.WriteAllLines(DataFileName, linesList);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading from file " + ex.Message);
            }
        }

        //add airport
        static void AddAirport()
        {
            Console.WriteLine("Adding an airport.");
            Console.Write("Enter airport code: ");
            string code = Console.ReadLine();
            Console.Write("Enter city: ");
            string city = Console.ReadLine();
            Console.Write("Enter latidude: ");
            string latStr = Console.ReadLine();
            if (!double.TryParse(latStr, out double lat))
            {
                Console.WriteLine("Latidude must be a valid integer");
                return;
            }
            Console.Write("Enter longitude: ");
            string lngStr = Console.ReadLine();
            if (!double.TryParse(lngStr, out double lng))
            {
                Console.WriteLine("Longitude must be a valid integer");
                return;
            }
            Console.Write("Enter elevation: ");
            string eleStr = Console.ReadLine();
            if (!int.TryParse(eleStr, out int elevM))
            {
                Console.WriteLine("Elevation must be a valid integer");
                return;
            }
            //
            try
            {
                Airport a = new Airport() { Code = code, City = city, Latitude = lat, Longitude = lng, elevationMeters = elevM };
                AirportsList.Add(a);
                Console.WriteLine("Airport added.");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Airport data invalid: " + ex.Message);
            }
        }

        //list airports
        static void ListAirports()
        {
            foreach (Airport a in AirportsList)
            {
                Console.WriteLine(a);
            }
        }

        //find nearest airport
        static void FindNearestAirport(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            if ((lat1 == lat2) && (lon1 == lon2))
            {
                return 0;
            }
            else
            {
                double theta = lon1 - lon2;
                double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
                dist = Math.Acos(dist);
                dist = rad2deg(dist);
                dist = dist * 60 * 1.1515;
                if (unit == 'K')
                {
                    dist = dist * 1.609344;
                }
                else if (unit == 'N')
                {
                    dist = dist * 0.8684;
                }
                return (dist);
            }
        }


        Console.WriteLine("Found nearest airport to be {Code}/{City} distance is {distance}");
        }

        //find airport elevation
        static void FindAirportElevation()
        {
 

            //Console.WriteLine("For all airports the standard deviation of their elevation is " + standardDeviationOfValues);
           // }
        }

        //change log
        static int ChangeLogs()
        {
            Console.Write(
@"Changing logging settings:
1. Logging to console
2. Logging to file
Enter your choices, comma-separated, empty for none: ");
            try
            {
                string choiceStr = Console.ReadLine();
                int choice = int.Parse(choiceStr);
                return choice;
            }
            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                {
                    Console.WriteLine("Invalid value entered\n");
                }
                else throw ex;
            }
            return -1;
        }
    

        //menu
        static int GetMenuChoice()
        {
            Console.Write(
@"What do you want to do?
1. Add Airport
2. List all airports
3. Find nearest airport by code
4. Find airport's elevation standard deviation
5. Print Nth super-Fibonacci
6. Change log delegates
0. Exit
Enter your choice: ");
            try
            {
                string choiceStr = Console.ReadLine();
                int choice = int.Parse(choiceStr);
                return choice;
            }
            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                {
                    Console.WriteLine("Invalid value entered\n");
                }
                else throw ex; 
            }
            return -1;
        }

        static void Main(string[] args)
        {
            
            try
            {
                ReadDataFromFile();
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
            finally
            {
                int choice;
                do
                {
                    choice = GetMenuChoice();
                    switch (choice)
                    {
                        case 1:
                            AddAirport();
                            break;
                        case 2:
                            ListAirports();
                            break;
                        case 3:
                            FindNearestAirport();
                            break;
                        case 4:
                            FindAirportElevation();
                            break;
                        case 5:
                            SuperFib FibMenu = new SuperFib();
                            break;
                        case 6:
                            ChangeLogs();
                            break;
                        case 0: // exit
                            break;
                        default:
                            Console.WriteLine("Invalid choice try again.");
                            break;
                    }
                } while (choice != 0);
                WriteDataToFile();
                Console.WriteLine("Data saved back to file.");
                Console.WriteLine("Goodbye");
            }
        }//end Main method
    }//end class Program
}//end namespace


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        private int[,] grades; //rectangular array of grades

        //auto-implement property constructor 
        public string CourseName { get; }

        //two-parameter constructor initiaizes
        //auto-implemented property CourseName and grade array
        public GradeBook(string name, int[,] gradesArray)
        {
            CourseName = name;//set CourseName to name
            grades = gradesArray;// initialize grades array
        }

        //welcome message
        public void DisplayMessage()
        {
            //auto-implemented property CourseName gets the name
            Console.WriteLine($"Welcometo the grade book for\n{CourseName}!\n");
        }

        //data operations
        public void ProcessGrades()
        {
            OutputGrades(); //output grades array

            //call methods GetMinimum and GetMaximum
            Console.WriteLine(
                $"\nLowest grade in the grade book is {GetMinimum()}" +
                $"\nHighest grade in the grade book is {GetMaximum()}");

            //output grade distribution chart 
            OutputBarChart();
        }

        public int GetMinimum()
        {
            //assume first element of grades array is smallest
            var lowGrade = grades[0, 0];

            //loop through elements of rectangular grades array
            foreach (var grade in grades)
            {
                //if grade less than lowGrade assign it to lowGrade
                if (grade < lowGrade)
                {
                    lowGrade = grade;
                }
            }
            return lowGrade; //return lowest grade
        }

        public int GetMaximum()
        {
            //assume first element is the largest
            var highGrade = grades[0, 0];
        //loop through elements of rectangular grade array
            foreach (var grade in grades)
            {
                //if grade is higher than highGrade, assign it to highGrade
                if (grade > highGrade)
                {
                    highGrade = grade;
                }
            }

            return highGrade;
        }

        //determine average grade for particular student
        public double GetAverage(int student)
        {
            //get the number of grades per student
            var gradeCount = grades.GetLength(1);
            var total = 0.0;//initialize total

            //sum grades for one student
            for (var exam=0; exam<gradeCount; ++exam)
            {
                total += grades[student, exam];
            }
            return total / gradeCount;//return average
        }

        //output bar chart displaying overall grade distribution
        public void OutputBarChart()
        {
            Console.WriteLine("Overall grade distribution");

            //store frequency of grades in each range of10 grades
            var frequency = new int[11];

            //for each grade in grade book increment the appropo frequency
            foreach (var grade in grades)
            {
                ++frequency[grade / 10];
            }

            //for each grde frequency display bar in chart
            for (var count =0; count < frequency.Length; ++count)
            {
                //output bar lable 
                if (count == 10)
                {
                    Console.WriteLine(" 100 ");
                }
                else
                {
                    Console.WriteLine($"{count * 10:D2}-{count * 10 + 9:D2}");
                }

                //display bar of asterisks
                for (var stars = 0; stars <frequency[count];++stars)
                {
                    Console.WriteLine("*");
                }
                Console.WriteLine();//start a new line of output
            }
        }

        public void OutputGrades()
        {
            Console.WriteLine("The grades are:\n");
            Console.WriteLine("    "); //align column heads

            //create column heading for each of the tests
            for (var test=0; test<grades.GetLength(1); ++test)
            {
                Console.WriteLine($"Test {test + 1}");
            }
            Console.WriteLine("Average"); //student average column
            //create row/columns of text representing array grades
            for (var student=0;student<grades.GetLength(0);++student)
            {
                Console.WriteLine($"Student {student + 1,2}");

                //output student grades
                for (var grade = 0; grade<grades.GetLength(1); ++grade)
                {
                    Console.WriteLine($"{grades[student, grade],8}");
                }

                //callmethod GetAverage to calculate students average grade
                //pass row number as the argument to Getaverage
                Console.WriteLine($"{GetAverage(student),9:F}");
            }
        }
    }
}

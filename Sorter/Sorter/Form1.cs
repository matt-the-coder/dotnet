﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sorter
{


    public partial class Form1 : Form
    {
        private static readonly List<string> nums = new List<string>();

        public Form1()
        {
            InitializeComponent();
            textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(textBox1_KeyUp);
            button1.Click += new System.EventHandler(button1_Click);

        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                nums.Add(textBox1.Text);
                textBox1.Text = String.Empty;

                e.Handled = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            {
                listBox1.Sorted = true;
                listBox1.Items.Clear();
                listBox1.Items.AddRange(nums.Cast<object>().ToArray());


            }

        }


    }
}

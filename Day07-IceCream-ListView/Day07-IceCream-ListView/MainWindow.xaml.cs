﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07_IceCream_ListView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void AddScoop_ButtonClick(object sender, RoutedEventArgs e)
        {
            if (lvFlav.SelectedIndex == -1)
            {
                lblError.Visibility = Visibility.Visible;
                return;
            }
            

            ListViewItem item = lvFlav.SelectedItem as ListViewItem;
            lvScoop.Items.Add(item.Content);
            //lblError.Visibility = Visibility.Hidden;
        }

        private void DeleteScoop_ButtonClick(object sender, RoutedEventArgs e)
        {
            if (lvScoop.SelectedIndex == -1)
            {
                lblError.Visibility = Visibility.Visible;
                return;
            }
            lvScoop.Items.RemoveAt(lvScoop.Items.IndexOf(lvScoop.SelectedItem));
            //lblError.Visibility = Visibility.Hidden;
        }

        private void ClearAll_ButtonClick(object sender, RoutedEventArgs e)
        {
            lvScoop.Items.Clear();
        }
    }
}

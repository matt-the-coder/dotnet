﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    using System;
    public class funcexer3
    {
        public static int Sum(int num1, int num2)
        {
            int total;
            total = num1 + num2;
            //string str = Convert.ToString(total);
            return total;
            
        }

        public static void Main()
        {
            Console.Write("\n\nFunction to calculate the sum of two numbers :\n");
            Console.Write("--------------------------------------------------\n");
            Console.Write("Enter a number: ");
            int num1 = int.Parse(Console.ReadLine());
            Console.Write("Enter another number: ");
            int num2 = int.Parse(Console.ReadLine());
            Console.WriteLine("\nThe sum of two numbers is : {0} \n", Sum(num1, num2));

            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}

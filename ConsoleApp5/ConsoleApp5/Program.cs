﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            int total = 0; // initialize sum of grades 
            int gradeCounter = 0; //initialize num of grades entered so far

            //input and output
            Console.Write("Enter grade or -1 to quit: ");
            int grade = int.Parse(Console.ReadLine());

            // loop until sentinel value
                while (grade != -1)
                {
                    total = total + grade; // add grade to total
                    gradeCounter = gradeCounter + 1; // increment counter

                    // input
                    Console.Write("Enter grade or -1 to quit: ");
                    grade = int.Parse(Console.ReadLine());

            }

            // if entered at least one grade... 
            if (gradeCounter != 0)
            {
                // average of grades
                double average = (double)total / gradeCounter;
                // display the total and average
                Console.WriteLine("\nTotal of the {gradeCounter} grades entered is {total}");
                Console.WriteLine($"Class average is ");
            }
            else // output error messag
            {
                Console.WriteLine("No grades were entered");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day09_Custom_Dialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Globals.friendList.Add(new Friend() { Name = "John", Age = 33 });
            Globals.friendList.Add(new Friend() { Name = "Marta", Age = 12 });
            Globals.friendList.Add(new Friend() { Name = "Hobbit", Age = 77 });
            lvFriends.ItemsSource = Globals.friendList;
        }

        private void AddFriend_MenuClick(object sender, RoutedEventArgs e)
        {
            AddEditDialogue dialogue = new AddEditDialogue(this);
            if (dialogue.ShowDialog() == true)
            {
                lvFriends.Items.Refresh();
            }
        }

        private void lvFriends_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Friend friend = lvFriends.SelectedItem as Friend;
            if (friend == null) return;
            AddEditDialogue dialogue = new AddEditDialogue(this, friend);
            if (dialogue.ShowDialog() == true)
            {
                lvFriends.Items.Refresh();
            }

        }
    }
}

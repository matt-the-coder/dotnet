﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day09_Custom_Dialog
{
    /// <summary>
    /// Interaction logic for AddEditDialogue.xaml
    /// </summary>
    public partial class AddEditDialogue : Window
    {
        Friend currentlyEditingFriend;
        public AddEditDialogue(Window owner, Friend friend = null)
        {
            Owner = owner;
            currentlyEditingFriend = friend;
            InitializeComponent();
            btnAddUpdate.Content = friend == null ? "Add Friend" : "Update Friend";
            if (friend != null)
            {
                tbName.Text = friend.Name;
                tbAge.Text = friend.Age + "";
            }
            
            
        }

        private void Save_ButtonClick(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            int age = int.Parse(tbAge.Text); //should be tryparse instead
            if (currentlyEditingFriend != null)//update
            {
                currentlyEditingFriend.Name = name;
                currentlyEditingFriend.Age = age;
            }
            else
            {//add
                Friend friend = new Friend() { Name = name, Age = age };
                Globals.friendList.Add(friend);
                DialogResult = true; //close window with succesful input
            }
        }
    }
}

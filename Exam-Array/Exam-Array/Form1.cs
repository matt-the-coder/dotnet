﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exam_Array
{
    public partial class Form1 : Form
    {
        int Min = 1;
        int Max = 10;
        int[] Arr1 = new int[20] { 2, 3, 4, 5, 5, 2, 8, 9, 3, 7, 2, 3, 4, 5, 5, 2, 8, 9, 3, 7 };
        int[] Arr2 = new int[0];
        //var frequency = new int[0];





        public Form1()
        {
            InitializeComponent();
        }

        static void Main(string[] args)
        {
            //array
            int[] Arr1 = { 1, 3, 4, 2, 2, 1, 5, 2, 1, 3, 3, 1, 2, 2, 3, 5, 6, 2, 4, 2 };
            var frequency = new int[6];//array of frequency counters

            for (var answer = 0; answer < Arr1.Length; ++answer)
            {
                try
                {
                    ++frequency[Arr1[answer]];
                }
                catch (IndexOutOfRangeException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine($"responses[{answer}] = {Arr1[answer]}\n");
                }
            }
            listBox1.Items.Add($"{"Rating"}{"Frequency",10}");

            for (var rating = 1; rating < frequency.Length; ++rating)
            {
                Console.WriteLine($"{rating,6}{frequency[rating],10");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(Arr1);
        }
    }
}
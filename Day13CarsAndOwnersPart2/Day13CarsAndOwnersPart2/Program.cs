﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day13CarsAndOwnersPart2
{
    public class Owner
    {
        public int Id { get; set; }
        [Required] // means not-null
        [StringLength(100)] // nvarchar(50)
        public string Name { get; set; } // up to 100 characters
        ICollection<Car> CarsCollection { get; set; }
    }

    public class Car
    {

        public int Id { get; set; }

        [Required] // means not-null
        [StringLength(100)] // nvarchar(50)
        public string MakeModel { get; set; }
        [Required] // means not-null
        public int YearOfProd { get; set; }
        Owner Owner;

    }

    class Program
    {
        static ParkingDbContext ctx;
        static int GetMenuChoice()
        {
            Console.Write(
@"What do you want to do?
1. List all cars and their owner
2. List all owners and their cars
3. Add a car (no owner)
4. Add an owner (no cars)
5. Assign car to an owner (or no owner)
6. Delete an owner with all cars they own
0. Quit
Choice: ");
            try
            {
                string choiceStr = Console.ReadLine();
                int choice = int.Parse(choiceStr);
                return choice;
            }
            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                {
                    Console.WriteLine("Invalid value entered\n");
                }
                else throw ex; // if we don't handle an exception we MUST throw it!
            }
            return -1;
        }

        static void Main(string[] args)
        {
            ctx = new ParkingDbContext();
            //Console.WriteLine("plz wait");

            //try
            //{
            //    ParkingDbContext ctx = new ParkingDbContext();
            //    // adding record to database (INSERT)
            //    Car c1 = new Car { MakeModel = "Test Car", YearOfProd = 1888 };
            //    ctx.Cars.Add(c1); // this is NOT insert
            //    ctx.SaveChanges();
            //    Console.WriteLine("Test Car added");
            //    //Console.WriteLine("Press any key to finish");
            //}
            //finally
            //{
            //    Console.WriteLine("Press any key to finish");
            //    Console.ReadKey();
            //}

            int choice;
            do
            {
                choice = GetMenuChoice();
                switch (choice)
                {
                    case 1:
                        ListCars();
                        break;
                    case 2:
                        ListOwners();
                        break;
                    case 3:
                        AddCar();
                        break;
                    case 4:
                        AddOwner();
                        break;
                    case 5:
                        AssignOwner();
                        break;
                    case 6:
                        DeleteOwner();
                        break;
                    case 0: // exit
                        break;
                    default: // ALWAYS have a default handler in switch/case
                        Console.WriteLine("Invalid choice try again.");
                        break;
                }
                Console.WriteLine();
            } while (choice != 0);
            //SaveAllPeopleToFile();

           
        }
        private static void ListCars()
        {
            var carsColl = from c in ctx.Cars select c;
            foreach (Car c in carsColl)
            {
                Console.WriteLine(c);
            }

        }

        private static void ListOwners()
        {
            var ownersColl = from o in ctx.Owners select o;
            foreach (Owner o in ownersColl)
            {
                Console.WriteLine(o);
            }
        }

        private static void AddCar()
        {
            Console.Write("Enter make/model: ");
            string makeModel = Console.ReadLine();
            Console.Write("Enter year of production: ");
            int yop = int.Parse(Console.ReadLine()); // FIXME
            Car car = new Car() { MakeModel = makeModel, YearOfProd = yop };
            ctx.Cars.Add(car);
            ctx.SaveChanges();
        }

        private static void AddOwner()
        {
            Console.Write("Enter Owner name: ");
            string name = Console.ReadLine();
            Owner owner = new Owner() { Name = name };
            ctx.Owners.Add(owner);
            ctx.SaveChanges();
        }

        private static void AssignOwner()
        {
            Console.Write("Enter Id of a car: ");
            int carId = int.Parse(Console.ReadLine()); // FIXME
            // Car car = (from c in ctx.Cars where c.Id == carId select c).FirstOrDefault<Car>();
            Car car = ctx.Cars.Find(carId);
            if (car == null)
            {
                Console.WriteLine("Car not found");
                return;
            }
        }

        private static void DeleteOwner()
        {
            Console.Write("Enter Owner name: ");
            string name = Console.ReadLine();
            Owner owner = new Owner() { Name = name };
            if (name != null)
            {
                ctx.Owners.Remove(owner); // schedule for deletion from database
                ctx.SaveChanges();
                Console.WriteLine("Record deleted");
            }
            else
            {
                Console.WriteLine("record to delete not found");
            }
        }
    }
}


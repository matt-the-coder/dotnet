﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exam1
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }



        private void Form1_Load(object sender, EventArgs e)
        {


        }

        private void inputBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void outputBox_TextChanged(object sender, EventArgs e)
        {

        }

        public static int FibonacciSeries(int n)
        {
            int firstnumber = 0, secondnumber = 1, result = 0;

            if (n == 0) return 0; //To return the first Fibonacci number
            if (n == 1) return 1; //To return the second Fibonacci number


            for (int i = 2; i <= n; i++)
            {
                result = firstnumber + secondnumber;
                firstnumber = secondnumber;
                secondnumber = result;
            }

            return result;
        }

        private void startBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (inputBox.Text == "" || inputBox.Text == null)
                {
                    MessageBox.Show("Please input a positive Integer", "Empty Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    inputBox.Clear();
                    inputBox.Focus();
                }
                else
                {
                    long theNum = Convert.ToInt64(inputBox.Text);

                    if (theNum == 0)
                    {
                        MessageBox.Show("Please input a positive Integer that is greater than 0", "Zero Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        inputBox.Clear();
                        inputBox.Focus();
                    }
                    else if (theNum < 0)
                    {
                        MessageBox.Show("Please input a positive Integer", "Negative Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        inputBox.Clear();
                        inputBox.Focus();
                    }
                    else
                    {
                        int firstnumber = 0, secondnumber = 1, result = 0;
                        long n = Convert.ToInt64(inputBox.Text);

                        for (int i = 2; i <= n; i++)
                        {
                            result = firstnumber + secondnumber;
                            firstnumber = secondnumber;
                            secondnumber = result;
                            this.listBox1.Items.Add(result);
                        }

                       // this.listBox1.Items.Add(result);

                    }
                }
            }


            catch (Exception err)
            {
                //MessageBox.Show("Error: Please input a positive Integer", err.Message);
                MessageBox.Show("Please input a positive Integer", "String or decimal Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                inputBox.Clear();
                inputBox.Focus();
            }

        }
    }
}

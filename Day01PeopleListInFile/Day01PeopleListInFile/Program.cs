﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{

    public class Person
    {
        //getters/setters
        private string _name;
        private int _age;
        private string _city;

        public string Name// Name 2-100 characters long, not containing semicolons
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                {
                    throw new ArgumentException("Name must be 2-100 characters long and not contain a ;");
                }
                value = _name;
            }

        }
        public int Age // Age 0-150
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 2 || value > 150)
                {
                    throw new ArgumentException("Age must be between 2 and 150");
                }
                value = _age;
            }

        }
        public string City // City 2-100 characters long, not containing semicolons
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                {
                    throw new ArgumentException("Name must be 2-100 characters long and not contain a ;");
                }
                value = _city;
            }

        }

        // end getters/setters

        public override string ToString()
        {
            //return string.Format("{0} is {1} from {2}");
            return $"{Name} is {Age} from {City}"; //string interpolation
        }

    }

    class Program
    {
        const string filePath = @"C:\Users\1896422\Documents\dotnet-github\Day01PeopleListInFile/people.txt";
        static List<Person> peopleList = new List<Person>();
        // public string filePath = @"c:\people.txt";

        static void AddPersonInfo()
        {
            int age;

            Console.WriteLine("Adding a person");
            Console.WriteLine("Enter a name");
            string name = Console.ReadLine();
            Console.WriteLine("Enter age");
            string ageStr = Console.ReadLine();
            if (!int.TryParse(ageStr, out age)) //make sure only int accepted
            {
                Console.WriteLine("Age must be integer");
                return;
            }
            Console.WriteLine("Enter city");
            string city = Console.ReadLine();

            //add person to list
            try
            {
                Person p = new Person() { Name = name, Age = age, City = city };
                peopleList.Add(p);
                Console.WriteLine("Person Added.");

            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Data Invalid" + ex.Message);
            }

        }

        static void ListAllPersonsInfo()
        {
            foreach (Person p in peopleList)
            {
                Console.WriteLine(p);
            }
        }//end method

        static void FindPersonByName() //IMPLEMENT USING LINQ
        {
            Console.WriteLine("Enter partial name to search for");
            string name = Console.ReadLine();
            var matchList = from p in peopleList where p.Name.ToUpper().Contains(name) select p;
            Console.WriteLine("Matching person: ");
            foreach (var m in matchList)
            {
                Console.WriteLine(m);
            }
        }//end method

        static void FindPersonYoungerThan() //IMPLEMENT USING LINQ
        {
            Console.WriteLine("Enter max age to search for: ");


        }//end method

        static void ReadAllPeopleFromFile()
        {

            peopleList.Clear();
            //if file not found
            if (!File.Exists(filePath))
            {
                return;
            }
            try
            {
                string[] linesArray = File.ReadAllLines(filePath);
                foreach (string line in linesArray)
                {
                    string[] data = line.Split(';');
                    if (data.Length != 3)
                    {
                        Console.WriteLine("Error. Invalid number of fields in line" + line);
                        continue;
                    }
                    string name = data[0];
                    string ageStr = data[1];
                    //parse the age
                    if (!int.TryParse(ageStr, out int age))
                    {
                        Console.WriteLine("Error: failed to parse age");
                        continue;
                    }
                    string city = data[2];
                    //create Person, add to list
                    try
                    {
                        Person p = new Person() { Name = name, Age = age, City = city };
                        peopleList.Add(p);

                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Person Data Invalid" + ex.Message);
                    }
                }

            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading from file");
            }

        }//end method

        static void SaveAllPeopleToFile()
        {
            List<string> lineList = new List<string>;


        }//end method

        // new menu
        static int GetMenuChoice()
        {
            Console.Write(
@"What do you want to do?
1. Add person info
2. List persons info
3. Find and list a person by name
4. Find and list persons younger than age
0. Exit
Choice: ");
            try
            {
                string choiceStr = Console.ReadLine();
                int choice = int.Parse(choiceStr);
                return choice;
            }
            catch (Exception ex)
            {
                if (ex is FormatException | ex is OverflowException)
                {
                    Console.WriteLine("Invalid value entered\n");
                }
                else throw ex;
            }
            return -1;
        }//end method

        static void Main(string[] args)
        {
            //old menu
            //Console.Write("What do you want to do? ");
            //Console.Write("1.Add person info ");
            //Console.Write("2.List persons info ");
            //Console.Write("3.Find a person by name ");
            //Console.Write("4.Find all persons younger than age ");
            //Console.Write("0.Exit ");

            ReadAllPeopleFromFile();
            int choice;
            do
            {
                //menu build
                choice = GetMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddPersonInfo();
                        break;
                    case 2:
                        ListAllPersonsInfo();
                        break;
                    case 3:
                        FindPersonByName();
                        break;
                    case 4:
                        FindPersonYoungerThan();
                        break;
                    case 0: //exit
                        break;
                    default:
                        Console.WriteLine("Invalid choice try again");
                        break;
                }
                Console.WriteLine(); //empty line 
            } while (choice != 0);
            SaveAllPeopleToFile();










        } //end method Main
    }//end class Program
}//end namespace

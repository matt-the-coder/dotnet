﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day10_PassengerThing
{
    /// <summary>
    /// Interaction logic for AddPassenger.xaml
    /// </summary>
    public partial class AddPassenger : Window
    {
        Passenger currentlyEditedPassenger;

        public AddPassenger(Window owner, Passenger passenger = null)
        {
            Owner = owner;
            currentlyEditedPassenger = passenger;
            InitializeComponent();
            btnUpdate.Content = passenger == null ? "Save" : "Update";
            if (passenger != null)
            {
                tbName.Text = passenger.Name;
                tbPassport.Text = passenger.Passport;
                tbDestination.Text = passenger.Destination;
                dpDepartureDate.Text = passenger.DepartureDate;
                cbDepartureTime.Text = passenger.DepartureTime + "";
            }
        }

        private void Cancel_ButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void Delete_ButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void SaveUpdate_ButtonClick(object sender, RoutedEventArgs e)
        {

        }
    }
}

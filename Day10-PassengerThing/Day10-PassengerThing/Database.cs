﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10_PassengerThing
{
    
    class Database
    {
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\1896422\Documents\passengersDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }

        public List<Passenger> GetAllPassengers()
        {
            List<Passenger> result = new List<Passenger>();
            SqlCommand command = new SqlCommand("SELECT * FROM passengersDB", conn);
            // Create new SqlDataReader object and read data from the command.
            using (SqlDataReader reader = command.ExecuteReader())
            {
                // while there is another record present
                while (reader.Read())
                {
                    //int id = (int)reader["Id"];
                    string name = (string)reader["Name"];
                    string passport = (string)reader["Passport"];
                    string destination = (string)reader["Destination"];
                    DateTime departureDate = (DateTime)reader["Departure Date"];
                    string departureTime = (string)reader["Departure Time"];

                    Passenger passenger = new Passenger() { Name = name, Passport = passport, Destination = destination, DepartureDate = departureDate, DepartureTime = departureTime };
                    result.Add(passenger);
                }
            }
            return result;
        }

        public void AddPassenger(Passenger passenger)
        {
            SqlCommand command = new SqlCommand("INSERT INTO passengersDB (Name, Passport, Destination, DepartureDate, DepartureTime) VALUES (@Name, @Passport, @Destination, @DepartureDate, @DepartureTime)", conn);
            command.Parameters.AddWithValue("@Name", passenger.Name);
            command.Parameters.AddWithValue("@Passport", passenger.Passport);
            command.Parameters.AddWithValue("@Destination", passenger.Destination);
            command.Parameters.AddWithValue("@DepartureDate", passenger.DepartureDate);
            command.Parameters.AddWithValue("@DepartureTime", passenger.DepartureTime);
            command.ExecuteNonQuery();
        }
    }
}

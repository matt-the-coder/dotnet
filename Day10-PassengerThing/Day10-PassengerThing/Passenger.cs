﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10_PassengerThing
{
    class Passenger
    {
        public string Name { get; set; }
        public string Passport { get; set; }
        public string Destination { get; set; }
        public DateTime DepartureDate { get; set; }
        public string DepartureTime { get; set; }

    }
}

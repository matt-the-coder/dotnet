﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10_PassengerThing
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.Db = new Database();
                lvPassengers.ItemsSource = Globals.Db.GetAllPassengers();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message);
                Close();//fatal error - closeapp
            }
        }

        private void AddPassenger_ButtonClick(object sender, RoutedEventArgs e)
        {
            AddPassenger dialog = new AddPassenger(this);
            if (dialog.ShowDialog() == true)
            { // only refresh if data changed
                lvPassengers.Items.Refresh();
            }
        }
    }
}

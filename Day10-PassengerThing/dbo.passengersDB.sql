﻿CREATE TABLE [dbo].[passengersDB]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(100) NOT NULL, 
    [Passport] NVARCHAR(50) NOT NULL, 
    [Destination] NVARCHAR(50) NOT NULL, 
    [DepartureDate] NVARCHAR(50) NOT NULL, 
    [DepartureTime] NVARCHAR(50) NOT NULL
)

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        public static int Sum(int num1, int num2)
        {
            int total;
            total = num1 + num2;
            return total;
        }

        static void Main(string[] args)
        {

            Console.Write("Enter your birth date: ");
            DateTime BirthDay = DateTime.Parse(Console.ReadLine());
            DateTime now = DateTime.Now;
            int age = (int)((now - BirthDay).TotalDays / 365.242199);
            Console.WriteLine("You are" + age + "year(s) old");
            Console.WriteLine("After ten years you will be at the age of " + (age + 10));
        }
    }
}

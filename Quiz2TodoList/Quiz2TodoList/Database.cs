﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2TodoList
{
    public class Database
    {
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\1896422\Documents\Quiz2DB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }

        public List<Todo> GetAllTodos(string order)
        {
            List<Todo> result = new List<Todo>();
            SqlCommand command = new SqlCommand("SELECT * FROM Todos ORDER BY " + order, conn);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string task = (string)reader["Task"];
                    DateTime dueDate = (DateTime)reader["DueDate"];
                    TaskStatus status = (TaskStatus)Enum.Parse(typeof(TaskStatus), (string)reader["Task Status"]);
                    Todo todo = new Todo() { Id = id, Task = task, DueDate = dueDate, Status = status, };
                    result.Add(todo);
                }
            }
            return result;
        }

        //Add
        public void AddTodo(Todo todo)
        {
            SqlCommand command = new SqlCommand("INSERT INTO Todos (Task, DueDate, TaskStatus,) VALUES (@Task, @DueDate, @TaskStatus)", conn);
            command.Parameters.AddWithValue("@Task", todo.Task);
            command.Parameters.AddWithValue("@DueDate", todo.DueDate);
            command.Parameters.AddWithValue("@TaskStatus", todo.Status.ToString());
            command.ExecuteNonQuery();
        }

        //Update
        public void UpdateTodo(Todo todo)
        {
            SqlCommand command = new SqlCommand("UPDATE Todos SET Task=@Task, DueDate=@DueDate Status=@TaskStatus, WHERE Id=@Id", conn);
            command.Parameters.AddWithValue("@Task", todo.Task);
            command.Parameters.AddWithValue("@DueDate", todo.DueDate);
            command.Parameters.AddWithValue("@TaskStatus", todo.Status.ToString());
            command.Parameters.AddWithValue("@Id", todo.Id);
            command.ExecuteNonQuery();
        }

        //Delete
        public void DeleteTodo(int id)
        {
            SqlCommand command = new SqlCommand("DELETE FROM Todos WHERE Id=@Id", conn);
            command.Parameters.AddWithValue("@Id", id);
            command.ExecuteNonQuery();
        }

    }
}

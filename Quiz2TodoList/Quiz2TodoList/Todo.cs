﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2TodoList
{
    public class Todo
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public TaskStatus Status { get; set; }
        public DateTime DueDate { get; set; }
    }

    public enum TaskStatus { Pending, Done }
}

﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2TodoList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string sortOrder = "Id";

        public MainWindow()
        {
            var listView1 = new ListView();
            DataTable table = new DataTable();
            foreach (ListViewItem item in listView1.Items)
            {
                table.Columns.Add(item.ToString());
                foreach (var it in item.SubItems)
                    table.Rows.Add(it.ToString());
            }
            InitializeComponent();

            // connect to DB
            try
            {
                Globals.Db = new Database();
                lvMain.ItemsSource = Globals.Db.GetAllTodos(sortOrder);
            }
            catch (SystemException ex) //(SqlException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message);
                Close();
            }
        }

        private void FileExport_MenuClick(object sender, RoutedEventArgs e)
        {

            //declare new SaveFileDialog + set it's initial properties 

            {
                SaveFileDialog sfd = new SaveFileDialog
                {
                    Title = "Choose file to save to",
                    FileName = "example.csv",
                    Filter = "CSV (*.csv)|*.csv",
                    FilterIndex = 0,
                    InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                };

                //show the dialog + display the results in a msgbox unless cancelled 


                if (sfd.ShowDialog() == DialogResult.OK)
                {

                    string[] headers = lvMain.Columns
                               .OfType<ColumnHeader>()
                               .Select(header => header.Text.Trim())
                               .ToArray();

                    string[][] items = lvMain.Items
                                .OfType<ListViewItem>()
                                .Select(lvi => lvi.SubItems
                                    .OfType<ListViewItem.ListViewSubItem>()
                                    .Select(si => si.Text).ToArray()).ToArray();

                    string table = string.Join(",", headers) + Environment.NewLine;
                    foreach (string[] a in items)
                    {
                        //a = a_loopVariable; 
                        table += string.Join(",", a) + Environment.NewLine;
                    }
                    table = table.TrimEnd('\r', '\n');
                    System.IO.File.WriteAllText(sfd.FileName, table);
                    tbxStatus.Text = "Item Exported";
                }
            }

        }


        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Add_ButtonClick(object sender, RoutedEventArgs e)
        {

            AddEditTodo dialog = new AddEditTodo(this);
            dialog.ShowDialog();
            
        }

        private void lvMain_RightClick(object sender, EventArgs e)
        {

            //MessageBox.Show(lvMain.SelectedItems.ToString());
            MessageBoxResult result = MessageBox.Show((lvMain.SelectedItems.ToString()) + "\n Do you want to delete this item?", "Item Deletion", MessageBoxButton.YesNo, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    MessageBox.Show("This Item was Deleted", "Item Deletion");
                    tbxStatus.Text = "Item Deleted";
                    break;
                case MessageBoxResult.No:
                    Close();
                    break;
            }
        }

        private void lvMain_DoubleClick(object sender, EventArgs e)
        {

            AddEditTodo dialog = new AddEditTodo(this);
            dialog.ShowDialog();
        }

        private void rbTaskSort_Checked(object sender, RoutedEventArgs e)
        {
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvMain.ItemsSource);
            view.SortDescriptions.Add(new SortDescription("Task", ListSortDirection.Ascending));

        }

        private void rbDateSort_Checked(object sender, RoutedEventArgs e)
        {
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvMain.ItemsSource);
            view.SortDescriptions.Add(new SortDescription("Due Date", ListSortDirection.Ascending));
        }

        //public void CreateCSVFile(DataTable dt, string strFilePath)
        //{
        //    StreamWriter sw = new StreamWriter(strFilePath, false);

        //    int iColCount = dt.Columns.Count;
        //    for (int i = 0; i < iColCount; i++)
        //    {
        //        sw.Write(dt.Columns[i]);
        //        if (i < iColCount - 1)
        //        {
        //            sw.Write(",");
        //        }
        //    }
        //    sw.Write(sw.NewLine);

        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        for (int i = 0; i < iColCount; i++)
        //        {
        //            if (!Convert.IsDBNull(dr[i]))
        //            {
        //                sw.Write(dr[i].ToString());
        //            }
        //            if (i < iColCount - 1)
        //            {
        //                sw.Write(",");
        //            }
        //        }
        //        sw.Write(sw.NewLine);
        //    }
        //    sw.Close();
        //}


        //SQL
//        CREATE TABLE[dbo].[Todos]
//        (

//   [Id] INT NOT NULL PRIMARY KEY, 
//    [Task] NVARCHAR(50) NOT NULL,
//    [DueDate] DATE NOT NULL, 
//    [TaskStatus] NVARCHAR(50) NOT NULL,
//    CHECK(TaskStatus IN('Done', 'Pending'))
//)
    }
}

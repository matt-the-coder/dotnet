﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2TodoList
{
    
    public partial class AddEditTodo : Window
    {

        string sortOrder = "Id";
        Todo currentlyEditedTodo;
        public AddEditTodo(Window owner, Todo todo = null)
        {
            Owner = owner;
            currentlyEditedTodo = todo;
            InitializeComponent();
            btnUpdate.Content = todo == null ? "Add" : "Update";
            if (todo != null)
            {
                tbTask.Text = todo.Task;
                dpDueDate.SelectedDate = todo.DueDate;
                cbIsDone.IsChecked = false;
            }
        }

        private void Cancel_ButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AddEdit_ButtonClick(object sender, RoutedEventArgs e)
        {
            //COMMENTED OUT BELOW
            //SPENT MOST OF THE TEST TRYING TO DEBUG THIS
            //USED CODE I USED BEFORE THAT WORKED, FROM HOMEWORK
            //COMMENTED OUT SO IT COMPILES, BUT ADD/UPDATE DOES NOT WORK
            //WITH THIS COMMENTED OUT
            //SORRY, MATT

            Button button = sender as Button;
            bool isUpdating = (button.Name == "btUpdate");

            string task = tbTask.Text;
            TaskStatus status = cbIsDone.IsChecked == true ? TaskStatus.Done : TaskStatus.Pending;
            DateTime dueDate = dpDueDate.SelectedDate.Value;

            try
            {
                if (isUpdating)
                {
                    Todo todo = lvMain.SelectedItem as Todo;
                    if (todo == null) return;
                    todo.Task = task;
                    todo.Status = status;
                    todo.DueDate = dueDate;
                    Globals.Db.UpdateTodo(todo);
                    tbxStatus.Text = "Item Updated";
                }
                else
                {

                    Todo todo = new Todo() { Task = task, Status = status, DueDate = dueDate };
                    Globals.Db.AddTodo(todo);
                    tbxStatus.Text = "Item Added";
                }
                lblID.Content = "-";
                tbTask.Text = "";
                dpDueDate.SelectedDate = DateTime.Now;
                cbIsDone.IsChecked = false;
                lvMain.ItemsSource = Globals.Db.GetAllTodos(sortOrder);
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message);
            }
            //try
            //{   // add/update database records

            //    string task = tbTask.Text;
            //    DateTime duedate = dpDueDate;
            //    string status = cbIsDone;
            //    Globals.Db.AddTodo(new Todo() { Task = task, DueDate = duedate, Status = status});
            //    DialogResult = true;
            //    if (task.Length < 1 || task.Length > 50)
            //    {
            //        MessageBox.Show("Task description must be between 1-50 characters long");
            //        return;
            //    }
            //}
            //catch (ArgumentException ex)
            //{
            //    MessageBox.Show("Internal error: " + ex.Message);
            //    return;
            //}
        }
    }
    
}

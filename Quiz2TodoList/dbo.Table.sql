﻿CREATE TABLE [dbo].[Todos]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Task] NVARCHAR(50) NOT NULL, 
    [DueDate] DATE NOT NULL, 
    [TaskStatus] NVARCHAR(50) NOT NULL,
	CHECK(TaskStatus IN ('Done', 'Pending'))
)

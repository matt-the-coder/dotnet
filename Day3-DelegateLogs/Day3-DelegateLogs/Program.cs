﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3_DelegateLogs
{
    class Program
    {
        public delegate void LoggerDelegate(string msg);

        public static void LogToScreen(string m)
        {
            Console.WriteLine("Screen: " + m);
        }

        public static void LogFancy(string mmm)
        {
            Console.WriteLine("Fancy: " + mmm);
        }

        public static void LogToFile(string message)
        {
            DateTime dt = DateTime.Now; //print time for each new line added to file
            File.AppendAllText(@"..\..\log.txt", $"{dt}: {message} {Environment.NewLine}");
        }


        static void Main(string[] args)
        {
            try
            {
                LoggerDelegate logger;

                logger = LogToScreen;
                logger += LogFancy;
                logger += LogToFile;

                logger("Something happened");
            }
            finally
            {
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
        }
    }
}

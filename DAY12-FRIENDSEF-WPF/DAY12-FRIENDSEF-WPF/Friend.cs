﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAY12_FRIENDSEF_WPF
{
    public class Friend
    {
        public int Id { get; set; }
        [Required] // means not null
        [StringLength(50)] //nvarchar(50)
        public string Name { get; set; }
        [Required] // means not null
        [StringLength(50)] //nvarchar(50)
        public int Age { get; set; }

        public override string ToString()
        {
            return $"{Id}: {Name} is {Age} y/o";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAY12_FRIENDSEF_WPF
{
    class CompanionDbContext : DbContext
    {
        virtual public DbSet<Friend> Friends { get; set; }
    }
}

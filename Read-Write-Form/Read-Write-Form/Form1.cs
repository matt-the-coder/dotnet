﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Read_Write_Form
{
    public partial class Form1 : Form
    {
        private void GetInformation(string fileName)
        {
            richTextBox1.Clear();

            // output that file or directory exists
            richTextBox1.AppendText($"{fileName} exists" +
               Environment.NewLine);

            // output when file or directory was created
            richTextBox1.AppendText(
               $"Created: {File.GetCreationTime(fileName)}" +
               Environment.NewLine);

            // output when file or directory was last modified
            richTextBox1.AppendText(
               $"Last modified: {File.GetLastWriteTime(fileName)}" +
               Environment.NewLine);

            // output when file or directory was last accessed
            richTextBox1.AppendText(
               $"Last accessed: {File.GetLastAccessTime(fileName)}" +
               Environment.NewLine);
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string filename = this.textBox1.Text;
            string text = this.textBox2.Text;

            string path = @"C:\test\" + filename;

            try
            {

                // Create the file.
                using (FileStream fs = File.Create(path))
                {
                    Byte[] info = new UTF8Encoding(true).GetBytes(text);
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                    MessageBox.Show("File created");
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string filename = this.textBox1.Text;
            string text = this.richTextBox1.Text;

            string path = @"C:\test\" + filename;

            try
            {

                // Open the stream and read it back.
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        this.richTextBox1.Text = s;
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string filename = this.textBox1.Text;

            string path = @"C:\test\" + filename;

            try
            {

                // Delete the file if it exists.
                if (File.Exists(path))
                {
                    File.Delete(path);
                    MessageBox.Show("File deleted");
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }


        }

        private void button5_Click(object sender, EventArgs e)
        {
            string filename = this.textBox1.Text;
            string newfilename = this.textBox2.Text;

            string path = @"C:\test\" + filename;
            string path2 = @"C:\test\" + newfilename;
            try
            {
                if (!File.Exists(path))
                {
                    // This statement ensures that the file is created,
                    // but the handle is not kept.
                    using (FileStream fs = File.Create(path)) { }
                }

                // Ensure that the target does not exist.
                if (File.Exists(path2))
                    File.Delete(path2);

                // Move the file.
                File.Move(path, path2);
                MessageBox.Show("renamed");


            }
            catch (Exception ex)
            {
                MessageBox.Show("The process failed", ex.ToString());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            {
                string path = this.textBox1.Text;

                try
                {

                    string[] allFoundFiles = Directory.GetFiles(@"C:\test\", $"{path}*", SearchOption.AllDirectories);
                    foreach (string file in allFoundFiles)
                    {
                        this.listBox1.Items.Add(file);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("The process failed: {0}", ex.ToString());
                }
                finally { }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.listBox1.Items.Clear();
        }
    }
}//end class

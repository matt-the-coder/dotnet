﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06_Temperature
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double tempurature;
            double celsius;
            double fahrenheit;

            celsius = double.Parse(tbInput.Text);
            fahrenheit = double.Parse(tbInput.Text);
            if (!double.TryParse(tbInput.Text, out tempurature) == false)
            {
                MessageBox.Show("ERROR: Please enter a numeric temperature to convert", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                tbInput.Focus();
            }

            fahrenheit = double.Parse(tbInput.Text);
            fahrenheit = (tbInput * 9.0 / 5.0 + 32);
            lblResults.Text = fahrenheit.ToString();
        }
    }
}

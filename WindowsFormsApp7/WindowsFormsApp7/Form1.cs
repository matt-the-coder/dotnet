﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp7
{
    public partial class Form1 : Form

    {
        private Random random = new Random();
        int[] arr1 = new int[20];
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 9; i++)   // populate the array  with 10 random values
            {
                int randomNumber = random.Next(1, 50);

                arr1[i] = randomNumber.ToString();
            }
        }
    }
}

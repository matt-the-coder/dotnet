﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3TripsWizardMatt
{
    class DBInitializer : CreateDatabaseIfNotExists<TripsDbContext>
    {
        protected override void Seed(TripsDbContext context)
        {
            IList<City> Citys= new List<City>();

            Citys.Add(new City() { Name = "Montreal", Latitude = 45.5017, Longitude= 73.5673 });
            Citys.Add(new City() { Name = "Toronto", Latitude = 43.6532, Longitude = 79.3832 });
            Citys.Add(new City() { Name = "Halifax", Latitude = 73.5673, Longitude = 63.5752 });
            Citys.Add(new City() { Name = "Ottawa", Latitude = 45.4215, Longitude = 75.6972 });
            Citys.Add(new City() { Name = "Winnipeg", Latitude = 49.8951, Longitude = 97.1384 });

            context.Cities.AddRange(Citys);

            base.Seed(context);
        }
    }
}

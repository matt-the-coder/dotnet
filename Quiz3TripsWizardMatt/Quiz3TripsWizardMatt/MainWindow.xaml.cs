﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz3TripsWizardMatt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            Globals.ctx = new TripsDbContext();
            
            //List Trips
            var tripCol = from p in Globals.ctx.Travellers select p;
            foreach (Traveller p in tripCol)
            {
                lvTrips.Items.Add(p);
            }

        }


        private void Export_MenuItemClick(object sender, RoutedEventArgs e)
        {

        }

        private void Add_MenuItemClick(object sender, RoutedEventArgs e)
        {
            SelectFromToCity dialog = new SelectFromToCity(this);
            if (dialog.ShowDialog() == true)
            { // only refresh if data changed
                lvTrips.Items.Refresh();
                var tripCol = from p in Globals.ctx.Travellers select p;
                foreach (Traveller p in tripCol)
                {
                    //Console.WriteLine(c);
                    lvTrips.Items.Remove(p);
                    lvTrips.Items.Add(p);
                }
            }
        }
    }
}


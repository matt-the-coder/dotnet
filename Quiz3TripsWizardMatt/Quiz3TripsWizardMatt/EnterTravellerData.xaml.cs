﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz3TripsWizardMatt
{
    /// <summary>
    /// Interaction logic for EnterTravellerData.xaml
    /// </summary>
    public partial class EnterTravellerData : Window
    {
        public EnterTravellerData()
        {
            InitializeComponent();

        }

        private void Cancel_ButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Finish_ButtonClick(object sender, RoutedEventArgs e)
        {
            var ctx = new TripsDbContext();

            string personName = tbName.Text;
            DateTime departureDate = DateTime.Parse(dpDeparture.Text);
            DateTime returnDate = DateTime.Parse(dpReturn.Text);

            Traveller traveller = new Traveller() { PersonName = personName, DepartureDate = departureDate, ReturnDate = returnDate };
            ctx.Travellers.Add(traveller);
            ctx.SaveChanges();
            DialogResult = true;
        }
    }
}

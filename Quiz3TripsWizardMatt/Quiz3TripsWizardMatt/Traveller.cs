﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3TripsWizardMatt
{
    public class Traveller
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string PersonName { get; set; } // 1-50 characters
        
        City FromCity { get; set; } // may be null
        
        City ToCity { get; set; } // may be null
        [Required]
        public DateTime DepartureDate { get; set; }
        [Required]
        public DateTime ReturnDate { get; set; }
        [Required]
        public double DistanceKm { get; set; }
    }
}

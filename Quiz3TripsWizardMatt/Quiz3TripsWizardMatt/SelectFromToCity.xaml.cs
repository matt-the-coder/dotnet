﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz3TripsWizardMatt
{
    /// <summary>
    /// Interaction logic for SelectFromToCity.xaml
    /// </summary>
    public partial class SelectFromToCity : Window
    {
        static TripsDbContext ctx;
        MainWindow mw;

        public SelectFromToCity(MainWindow owner)
        {
            mw = owner;
            Owner = owner;
            InitializeComponent();

            var cityCol = from c in ctx.Cities select c;
            foreach (City c in cityCol)
            {
                lvFromCity.Items.Add(c);
                lvToCity.Items.Add(c);
            }

            if (lvFromCity.SelectedItem == lvToCity.SelectedItem)
            {
                MessageBox.Show("Departure and Destination Cities Cannot be the same");
                return;
            }
        }
        private void Cancel_ButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Next_ButtonClick(object sender, RoutedEventArgs e)
        {
        
        //string fromCity = lvFromCity.SelectedItem;
        //    ((EnterTravellerData)Application.Current.MainWindow).lblFromCity = lvFromCity.SelectedItem;
        }
    }
}

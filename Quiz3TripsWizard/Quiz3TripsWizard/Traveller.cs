﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3TripsWizard
{
    public class Traveller
    {
        int Id { get; set; }
        [Required]
        [StringLength(50)]
        string PersonName { get; set; } // 1-50 characters
        [Required]
        City FromCity { get; set; } // may be null
        [Required]
        City ToCity { get; set; } // may be null
        [Required]
        DateTime DepartureDate { get; set; }
        [Required]
        DateTime ReturnDate { get; set; }
        [Required]
        double DistanceKm { get; set; }
    }
}

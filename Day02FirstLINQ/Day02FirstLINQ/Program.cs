﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02FirstLINQ
{
    class Program
    {
        static List<string> nameList = new List<string>();


        static void Main(string[] args)
        {
            try
            {
                //add name to list
                while (true)
                {
                    Console.WriteLine("Enter name: ");
                    string name = Console.ReadLine();
                    if (name == "")
                    {
                        break;
                    }
                    nameList.Add(name);
                }
                //linq
                Console.WriteLine("Enter Search: ");
                string search = Console.ReadLine().ToUpper();

                //var foundList = from n in nameList where n.ToUpper().Contains(search) select n;
                var foundList = nameList.Where(n => n.ToUpper().Contains(search));

                //sort list alphabetically with linq
                //nameList.OrderBy(x => x).ToList();
                //nameList.Sort();  good but not LINQ
                var sortedList = nameList.OrderBy(item => item);

                //second way to do it with linq
                // var sortedList = from n in nameList orderby n select n;

                Console.Write("Matching Names: ");
                foreach (string name in sortedList)
                {
                    Console.WriteLine(name);
                }


            }
            finally
            {
                Console.WriteLine("Press key to exit");
                Console.ReadKey();
            }
        }

    }
}

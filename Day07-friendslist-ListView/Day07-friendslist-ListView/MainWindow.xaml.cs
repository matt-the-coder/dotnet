﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07_friendslist_ListView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Friend> friendList = new List<Friend>();

        public MainWindow()
        {
            InitializeComponent();
            lvFriends.ItemsSource = friendList;
            LoadDataFromFile();
        }

        private void AddFriend_ButtonClick(object sender, RoutedEventArgs e)
        {
            //lvFriends.Items.Add(tbName.Text);
            string name = tbName.Text;
            int age = int.Parse(tbAge.Text);
            Friend friend = new Friend() { Name = name, Age = age };
            friendList.Add(friend);
            lvFriends.Items.Refresh();
            //TODO handle exceptions in setters, show message box in error
        }

        private void SaveDataToFile()
        {
            //TODO
        }

        private void LoadDataFromFile()
        {
            //TODO
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            SaveDataToFile();
        }

        private void tbAge_TextChanged(object sender, TextChangedEventArgs e)
        {
            lblNameError.Visibility = Friend.IsNameValid(tbName.Text) ? Visibility.Hidden : Visibility.Visible;
        }

        private void tbName_TextChanged(object sender, TextChangedEventArgs e)
        {
            lblAgeError.Visibility = Friend.IsAgeValid(tbAge.Text) ? Visibility.Hidden : Visibility.Visible;
        }
    }
}

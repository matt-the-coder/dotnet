﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03ArrayContains
{
    class Program
    {

        public static void PrintDups(int[,] a1, int[,] a2)
        {
            foreach (int v1 in a1)
            {
                foreach (int v2 in a2)
                {
                    if (v1 == v2)
                    {
                        //Console.WriteLine($"{v1}, ");

                    }
                }
            }
        }

        //public int[] Concatenate(int[] a1, int[] a2)
        //{


        // }

        static void Main(string[] args)
        {
            int[,] data1 = { { 2, 3 }, { 7, 9 }, { 3, 17 } };
            int[,] data2 = { { 2, 3, 77 }, { 7, 8, 3 } };
            int[,] test;
            PrintDups(data1, data2);

            //unique
            // var distinct = test.SelectMany(a => a).Distinct().ToArray();

            //print one by one
            Console.WriteLine("Print each one by one");
            var rowCount = data1.GetLength(0);
            var colCount = data1.GetLength(1);
            for (int row = 0; row < rowCount; row++)
            {
                for (int col = 0; col < colCount; col++)
                    Console.Write(String.Format("{0}\t", data1[row, col]));
                Console.WriteLine();
            }

            var rowCount2 = data2.GetLength(0);
            var colCount2 = data2.GetLength(1);
            for (int row = 0; row < rowCount2; row++)
            {
                for (int col = 0; col < colCount2; col++)
                    Console.Write(String.Format("{0}\t", data2[row, col]));
                Console.WriteLine();
                Console.ReadKey();
            }
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommissionEmployee
{
    public class CommissionEmployee : object
    {
        public string FirstName { get; }
        public string LastName { get; }
        public string SocialSecurityNumber { get; }
        private decimal GrossSales;
        private decimal CommissionRate;

        //five-parameter constructor
        public CommissionEmployee(string firstName, string lastName, string socialSecurityNumber, decimal grossSales, decimal commissionRate)
        {
            //implicit call toobject constructor
            FirstName = firstName;
            LastName = lastName;
            SocialSecurityNumber = socialSecurityNumber;
            GrossSales = grossSales;
            CommissionRate = commissionRate;
        }

        //propert that gets and sets commission employee's gross sales
        public decimal GrossSales
        {
            get
            {
                return grossSales
            }
            set
            {
                if (value <0)//validation
                {
                    throw new ArgumentOutOfRangeException(nameof(value), value,$"{nameof(GrossSales)} must be >= 0");
                }
                grossSales = value;
            }
        }

        public decimal CommissionRate
        {
            get
            {
                return commissionRate;
            }
            set
            {
                if (value <= 0 || value >=1) //validation
                {
                    throw new ArgumentOutOfRangeException(nameof(value), value, $"{nameof(CommissionRate)} must be > 0 and < 1");
                }
                commissionRate = value;
            }
        }

        public decimal Earnings() => commissionRate * grossSales;

        public override string ToString() =>
            $"comission employee: {FirstName} {LastName}\n" +
            $"social security number: {SocialSecurityNumber}\n" +
            $"gross sales: {grossSales:C}\n" +
            $"commission rate: {CommissionRate:F2}";




    }
}

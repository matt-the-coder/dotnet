﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsAppDB
{
    public partial class Form1 : Form
    {
        //global variables to create a connection between the C# project and the Database.
        private string server;
        private string database;
        private string uid;
        private string password;
        private MySqlConnection connection;
        //load the data adapter.
        private MySqlDataAdapter mySqlDataAdapter;
        private object cmd;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //initialize the connection
            server = "localhost";
            database = "customer_db";
            uid = "root";
            password = "root";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);

            if (this.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter("select * from customer_table", connection);
                //load the table which is held by the mySqlDataAdapter
                DataSet DS = new DataSet();
                //“fill” the DataSet with table connected to the mySqlDataAdapter
                mySqlDataAdapter.Fill(DS);
                //assign the prepared data source to the DaraGridView control.
                dataGridView1.DataSource = DS.Tables[0];

                //close connection
                this.CloseConnection();
            }

        }

        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            //handling errors
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server. Contact administrator");
                        break;
                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                    default:
                        MessageBox.Show(ex.Message);
                        break;
                }
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        //Updating the MySQL table via the DataGridView control

        //Go to the Events section of the properties window for the DataGridView control and generate the code chunk for the RowValidated event.

        private void dataGridView1_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            //commit the changes to the MySQL table
            DataTable changes = ((DataTable)dataGridView1.DataSource).GetChanges();

            if (changes != null) //precaution to avoid the NullReferenceException.
            {
                // instance of the MySqlCommandBuilder class
                //created with our mySqlDataAdapter as a parameter.
                MySqlCommandBuilder mcb = new MySqlCommandBuilder(mySqlDataAdapter);
                //use the instance of the MySqlCommandBuilder to
                //create an Update command and assign it to our mySqlDataAdapter.
                mySqlDataAdapter.UpdateCommand = mcb.GetUpdateCommand();
                //update MySQL database.
                mySqlDataAdapter.Update(changes);
                //tell the DataGridView control that we have committed the changes it had
                ((DataTable)dataGridView1.DataSource).AcceptChanges();

            }
        }

        private void dataGridView1_MouseUp(object sender, MouseEventArgs e)
        {
            textBox1.Text = dataGridView1[0, dataGridView1.CurrentRow.Index].Value.ToString();
            textBox2.Text = dataGridView1[1, dataGridView1.CurrentRow.Index].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //This is my connection string i have assigned the database file address path  
                string MyConnection2 = "datasource=localhost;port=3306;username=root;password=root";
                //This is my update query in which i am taking input from the user through windows forms and update the record.  
                string Query = "update customer_db set first_name='" + this.textBox1.Text;
                //This is  MySqlConnection here i have created the object and pass my connection string.  
                MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                MessageBox.Show("Data Updated");
                while (MyReader2.Read())
                {
                }
                MyConn2.Close();//Connection closed here  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }//end form


}//end class

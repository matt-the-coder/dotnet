﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08_FriendsDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.Db = new Database();
                lvFriends.ItemsSource = Globals.Db.GetAllFriends();
            }catch (SqlException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message);
                Close();//fatal error - closeapp
            }
        }

        private void AddFriend_ButtonClick(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string ageStr = tbAge.Text;
            if (!int.TryParse(ageStr, out int age))
            {
                MessageBox.Show("Age must be an int");
                return;
            }
            Friend friend = new Friend() { Name = name, Age = age };
            try
            {
                Globals.Db.AddFriend(friend);
                tbName.Text = "";
                tbAge.Text = "";
                lvFriends.ItemsSource = Globals.Db.GetAllFriends();
            } catch (SqlException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message);
                
            }
        }

        private void UpdateFriend_ButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void DeleteFriend_ButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            int total = 0; //sum of grades
            int gradeCounter = 0; //num of grades
            // grade count
            int aCount = 0; 
            int bCount = 0;
            int cCount = 0;
            int dCount = 0;
            int fCount = 0;

            Console.WriteLine("Enter the grade");
            Console.WriteLine("Ctrl Z and Enter to end program");
            //input
            string input = Console.ReadLine(); 

            //loop
            while (input != null)
            {
                int grade = int.Parse(input); //grade from input
                total += grade; //add grade to total
                ++gradeCounter; //increment
                //switch
                switch (grade / 10)
                {
                    case 9: //90
                    case 10: //100
                        ++aCount;
                        break;
                    case 8: //between 80 and 89
                        ++bCount;
                        break;
                    case 7: //between 70 and 79
                        ++cCount;
                        break;
                    case 6: //between 60 and 69
                        ++dCount;
                        break;
                    default: //less than 60
                        ++fCount;
                        break;
                }//end switch
                input = Console.ReadLine(); //user input
            }//end while
            Console.WriteLine("\nGrades");

            if (gradeCounter != 0)
            {
                double average = (double)total / gradeCounter; //calc average

                //output results
                Console.WriteLine($"Total of {gradeCounter} grades entered is {total}");
                Console.WriteLine($"Class average is {average:F}");
                Console.WriteLine("Number of students who received each grade:");
                Console.WriteLine($"A: {aCount}");
                Console.WriteLine($"B: {bCount}");
                Console.WriteLine($"C: {cCount}");
                Console.WriteLine($"D: {dCount}");
                Console.WriteLine($"F: {fCount}");
            }
            else //no grades inputed
            {
                Console.WriteLine("Error. No grades have been inputed");
            }//end if-else
            Console.WriteLine($"Total of {gradeCounter} grades entered is {total}");
            //Console.WriteLine($"Class average is {average:F}");
            Console.WriteLine("Number of students who received each grade:");
            Console.WriteLine($"A: {aCount}");
            Console.WriteLine($"B: {bCount}");
            Console.WriteLine($"C: {cCount}");
            Console.WriteLine($"D: {dCount}");
            Console.WriteLine($"F: {fCount}");
        }//end method
    }//end class
}//end namespace

﻿
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06_Notepad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string filePath;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void FileSave_MenuClick(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                if (saveFileDialog.ShowDialog() == true)
                {
                    File.WriteAllText(saveFileDialog.FileName, tbEditor.Text);
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error opening file" + ex.Message,
                    "Sharpnotepad", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    private void FileNew_MenuClick(object sender, RoutedEventArgs e)
        {
          
        }

        private void FileOpen_MenuClick(object sender, RoutedEventArgs e)
        {
            try
            {
                string directoryPath = System.IO.Path.GetDirectoryName(filePath);
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.DefaultExt = "txt";
                openFileDialog.Title = "AwfulPad: Browse Text Files";
                openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                if (openFileDialog.ShowDialog() == true)
                {
                    tbEditor.Text = File.ReadAllText(openFileDialog.FileName);
                }
                //sbStatus.Text = directoryPath;
            } catch (IOException ex)
                {
                MessageBox.Show("Error opening file" + ex.Message,
                    "Sharpnotepad", MessageBoxButton.OK, MessageBoxImage.Error);
                }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day12_FirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            try
            {
                SocietyDBContext ctx = new SocietyDBContext();

                Person p1 = new Person { Name = "Tom", Age = random.Next(100) };
                ctx.People.Add(p1);
                ctx.SaveChanges();
                Console.WriteLine("Record Added");

                //update - fecth then update
                var p2 = (from p in ctx.People where p.Id == 3 select p).FirstOrDefault<Person>();
                if (p2 != null)
                {
                    p2.Name = "Alison";
                    ctx.SaveChanges();
                } else
                {
                    Console.WriteLine("Record To Update Not Found");
                }

                //delete - fecth then delete
                var p3 = (from p in ctx.People where p.Id == 4 select p).FirstOrDefault<Person>();
                if (p3 != null)
                {
                    ctx.People.Remove(p3); //schedule for deletion
                    ctx.SaveChanges();
                    Console.WriteLine("Record Deleted");
                }
                else
                {
                    Console.WriteLine("Record To Delete Not Found");
                }



                //print all records
                var peopleCol = from p in ctx.People select p;
                foreach(Person p in peopleCol)
                {
                    Console.WriteLine($"{p.Id}: {p.Name} is {p.Age} y/o");
                }
            } finally
            {
                Console.WriteLine("Press Key");
                Console.ReadLine();
            }
        }
    }
}

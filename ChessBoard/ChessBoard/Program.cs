﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessBoard
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("-----------------------------");
            Console.WriteLine("|Print A Chess Board Pattern|");
            Console.WriteLine("-----------------------------");
            string[,] chess = new string[8, 8];
            int[,] chessBoard = new int[,] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } };
            string finalvalofcolumn = string.Empty;

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {

                    chess[i, j] = ((i + j) % 2 == 0) ? chess[i, j] = "O" : chess[i, j] = "X";

                }
            }

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Console.Write(chess[i, j], "");
                }
                Console.WriteLine("\n");
            }
            Console.ReadKey();

        }//end method
    }//end class Program
}//end class
